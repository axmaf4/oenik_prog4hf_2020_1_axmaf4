﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MonitorShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using MonitorShop.Data;

    /// <summary>
    /// The general repository interface.
    /// Contains the get one and get all functions.
    /// </summary>
    /// <typeparam name="T"> A Table type (class type). </typeparam>
    public interface IRepository<T>
        where T : class
    {
        /// <summary>
        /// Gets a general instance by id.
        /// </summary>
        /// <param name="id">The chosen id.</param>
        /// <returns>Returns the chosen instance.</returns>
        T GetById(int id);

        /// <summary>
        /// Gets all the instances from the database.
        /// </summary>
        /// <returns>Returns all the monitorbrand records as a list.</returns>
        IList<T> GetAll();
    }

    /// <summary>
    /// The interface of the monitor repository.
    /// </summary>
    public interface IMonitorRepository : IRepository<MONITOR>
    {
        /// <summary>
        /// Inserts a monitor to the database.
        /// </summary>
        /// <param name="monitor">the monitor to be inserted.</param>
        void CreateMonitor(MONITOR monitor);

        /// <summary>
        /// Gets a monitor by id.
        /// If it doesnt exist then throws an exception.
        /// </summary>
        /// <param name="id">The chosen monitors id.</param>
        /// <returns>Returns a monitor as a class.</returns>
        MONITOR GetById(int id);

        /// <summary>
        /// Gets all the monitors from the database.
        /// </summary>
        /// <returns>Returns all the monitor records as a list.</returns>
        IList<MONITOR> GetAll();

        /// <summary>
        /// Updates a monitors diameter in the database.
        /// If it doesnt exist then throws an exception.
        /// </summary>
        /// <param name="id">The chosen monitors id.</param>
        /// <param name="inch">The new diameter.</param>
        void UpdateKepatlo(int id, int inch);

        /// <summary>
        /// Updates a monitors screen ratio in the database.
        /// If it doesnt exist then throws an exception.
        /// </summary>
        /// <param name="id">The chosen monitors id.</param>
        /// <param name="wh">The new Width and Height as ratios.</param>
        void UpdateKeparany(int id, string wh);

        /// <summary>
        /// Updates a monitors resolution in the database.
        /// If it doesnt exist then throws an exception.
        /// </summary>
        /// <param name="id">The chosen monitors id.</param>
        /// <param name="wh">The new Width and Height.</param>
        void UpdateFelbontas(int id, string wh);

        /// <summary>
        /// Updates a monitors refresh rate in the database.
        /// If it doesnt exist then throws an exception.
        /// </summary>
        /// <param name="id">The chosen monitors id.</param>
        /// <param name="hz">The new refresh rate.</param>
        void UpdateKepfrissites(int id, int hz);

        /// <summary>
        /// Updates a monitors response time in the database.
        /// If it doesnt exist then throws an exception.
        /// </summary>
        /// <param name="id">The chosen monitors id.</param>
        /// <param name="ms">The new response time.</param>
        void UpdateValaszido(int id, int ms);

        /// <summary>
        /// Updates a monitors price in the database.
        /// If it doesnt exist then throws an exception.
        /// </summary>
        /// <param name="id">The chosen monitors id.</param>
        /// <param name="price">The new price.</param>
        void UpdateAr(int id, int price);

        /// <summary>
        /// Deletes a monitor from the database.
        /// </summary>
        /// <param name="id">The chosen monitors id.</param>
        void DeleteMonitor(int id);
    }

    /// <summary>
    /// The interface of the monitorbrand repository.
    /// </summary>
    public interface IMonitorbrandRepository : IRepository<MONITORBRAND>
    {
        /// <summary>
        /// Inserts a monitorbrand to the database.
        /// </summary>
        /// <param name="monitorbrand">the monitorbrand to be inserted.</param>
        void CreateMonitorBrand(MONITORBRAND monitorbrand);

        /// <summary>
        /// Gets a monitorbrand by id.
        /// If it doesnt exist then throws an exception.
        /// </summary>
        /// <param name="id">The chosen monitorbrands id.</param>
        /// <returns>Returns the chosen monitorbrand as a class.</returns>
        MONITORBRAND GetById(int id);

        /// <summary>
        /// Gets all the monitorbrands from the database.
        /// </summary>
        /// <returns>Returns all the monitorbrand records as a list.</returns>
        IList<MONITORBRAND> GetAll();

        /// <summary>
        /// Updates a monitorbrands name.
        /// If it doesnt exist then throws an exception.
        /// </summary>
        /// <param name="id">The chosen monitorbrands id.</param>
        /// <param name="name">The new name.</param>
        void UpdateBrandNev(int id, string name);

        /// <summary>
        /// Updates a monitorbrands foundation year.
        /// If it doesnt exist then throws an exception.
        /// </summary>
        /// <param name="id">The chosen monitorbrands id.</param>
        /// <param name="year">The new year.</param>
        void UpdateAlapitasEve(int id, int year);

        /// <summary>
        /// Updates a monitorbrands country.
        /// If it doesnt exist then throws an exception.
        /// </summary>
        /// <param name="id">The chosen monitorbrands id.</param>
        /// <param name="country">The new country.</param>
        void UpdateOrszag(int id, string country);

        /// <summary>
        /// Deletes a monitor from the database.
        /// </summary>
        /// <param name="id">The chosen monitorbrands id.</param>
        void DeleteMonitorBrand(int id);
    }

    /// <summary>
    /// The interface of the raktarinfo repository.
    /// </summary>
    public interface IRaktarinfoRepository : IRepository<RAKTARINFO>
    {
        /// <summary>
        /// Inserts a raktarinfo to the database.
        /// </summary>
        /// <param name="raktarinfo">the raktarinfo to be inserted.</param>
        void CreateRaktarinfo(RAKTARINFO raktarinfo);

        /// <summary>
        /// Gets a raktarinfo by id if exists.
        /// </summary>
        /// <param name="id">The chosen raktarinfo id.</param>
        /// <returns>Returns the chosen raktarinfo as a class.</returns>
        RAKTARINFO GetById(int id);

        /// <summary>
        /// Gets all the raktarinfos from the database.
        /// </summary>
        /// <returns>Returns all the raktarinfo records as a list.</returns>
        IList<RAKTARINFO> GetAll();

        /// <summary>
        /// Update a raktarinfos darabszam.
        /// If it doesnt exist then throws an exception.
        /// </summary>
        /// <param name="id">The chosen raktarinfos id.</param>
        /// <param name="cnt">The new count.</param>
        void UpdateDarabszam(int id, int cnt);

        /// <summary>
        /// Update a raktarinfos orderability.
        /// </summary>
        /// <param name="id">The chosen raktarinfos id.</param>
        /// <param name="orderable">The new value.</param>
        void UpdateRendelheto(int id, bool orderable);

        /// <summary>
        /// Deletes a raktarinfo from the database.
        /// </summary>
        /// <param name="id">The chosen raktarinfo id.</param>
        void DeleteRaktarinfo(int id);
    }
}
