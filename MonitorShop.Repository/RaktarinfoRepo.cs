﻿// <copyright file="RaktarinfoRepo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MonitorShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using MonitorShop.Data;

    /// <summary>
    /// The raktarinfos repository layer class.
    /// This accesses the database.
    /// </summary>
    public class RaktarinfoRepo : IRaktarinfoRepository
    {
        private MonitorDatabaseEntities db;

        /// <summary>
        /// Initializes a new instance of the <see cref="RaktarinfoRepo"/> class.
        /// </summary>
        /// <param name="db">The database to work on.</param>
        public RaktarinfoRepo(MonitorDatabaseEntities db)
        {
            this.db = db;
        }

        /// <summary>
        /// Inserts a raktarinfo to the database.
        /// </summary>
        /// <param name="raktarinfo">the raktarinfo to be inserted.</param>
        public void CreateRaktarinfo(RAKTARINFO raktarinfo)
        {
            try
            {
                this.db.RAKTARINFO.Add(raktarinfo);
                this.db.SaveChanges();
            }
            catch (System.ArgumentNullException)
            {
                throw new Exception("There is no monitor with that ID. You need to create a monitor first!");
            }
        }

        /// <summary>
        /// Deletes a raktarinfo from the database.
        /// </summary>
        /// <param name="id">The chosen raktarinfo id.</param>
        public void DeleteRaktarinfo(int id)
        {
            try
            {
                this.db.RAKTARINFO.Remove(this.GetById(id));
                this.db.SaveChanges();
            }
            catch (System.ArgumentNullException)
            {
                throw new Exception("There is no record with that ID");
            }
        }

        /// <summary>
        /// Gets all the raktarinfos from the database.
        /// </summary>
        /// <returns>Returns all the raktarinfo records as a list.</returns>
        public IList<RAKTARINFO> GetAll()
        {
            return this.db.RAKTARINFO.ToList();
        }

        /// <summary>
        /// Gets a raktarinfo by id if exists.
        /// </summary>
        /// <param name="id">The chosen raktarinfo id.</param>
        /// <returns>Returns the chosen raktarinfo as a class.</returns>
        public RAKTARINFO GetById(int id)
        {
            try
            {
                return this.db.RAKTARINFO.Where(x => x.monitorId == id).FirstOrDefault();
            }
            catch (System.ArgumentNullException)
            {
                throw new Exception("There is no record with that ID");
            }
        }

        /// <summary>
        /// Update a raktarinfos darabszam.
        /// If it doesnt exist then throws an exception.
        /// </summary>
        /// <param name="id">The chosen raktarinfos id.</param>
        /// <param name="cnt">The new count.</param>
        public void UpdateDarabszam(int id, int cnt)
        {
            try
            {
                var raktarinfo = this.GetById(id);
                raktarinfo.darabszam = cnt;
                this.db.SaveChanges();
            }
            catch (System.ArgumentNullException)
            {
                throw new Exception("There is no record with that ID");
            }
        }

        /// <summary>
        /// Update a raktarinfos orderability.
        /// </summary>
        /// <param name="id">The chosen raktarinfos id.</param>
        /// <param name="orderable">The new value.</param>
        public void UpdateRendelheto(int id, bool orderable)
        {
            try
            {
                var raktarinfo = this.GetById(id);
                raktarinfo.rendelheto = orderable;
                this.db.SaveChanges();
            }
            catch (System.ArgumentNullException)
            {
                throw new Exception("There is no record with that ID");
            }
        }
    }
}
