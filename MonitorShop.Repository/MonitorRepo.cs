﻿// <copyright file="MonitorRepo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MonitorShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MonitorShop.Data;

    /// <summary>
    /// The monitors repository layer class.
    /// This accesses the database.
    /// </summary>
    public class MonitorRepo : IMonitorRepository
    {
        private MonitorDatabaseEntities db;

        /// <summary>
        /// Initializes a new instance of the <see cref="MonitorRepo"/> class.
        /// </summary>
        /// <param name="db">The database to work on.</param>
        public MonitorRepo(MonitorDatabaseEntities db)
        {
            this.db = db;
        }

        /// <summary>
        /// Inserts a monitor to the database.
        /// </summary>
        /// <param name="monitor">the monitor to be inserted.</param>
        public void CreateMonitor(MONITOR monitor)
        {
            try
            {
                this.db.MONITOR.Add(monitor);
                this.db.SaveChanges();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Deletes a monitor from the database.
        /// </summary>
        /// <param name="id">The chosen monitors id.</param>
        public void DeleteMonitor(int id)
        {
            try
            {
                this.db.MONITOR.Remove(this.GetById(id));
                this.db.SaveChanges();
            }

            // catch (System.ArgumentNullException)
            catch (Exception e)
            {
                // throw new DatabaseException("There is no record with that ID");
                throw e;
            }
        }

        /// <summary>
        /// Gets all the monitors from the database.
        /// </summary>
        /// <returns>Returns all the monitor records as a list.</returns>
        public IList<MONITOR> GetAll()
        {
            return this.db.MONITOR.ToList();
        }

        /// <summary>
        /// Gets a monitor by id.
        /// If it doesnt exist then throws an exception.
        /// </summary>
        /// <param name="id">The chosen monitors id.</param>
        /// <returns>Returns a monitor as a class.</returns>
        public MONITOR GetById(int id)
        {
            try
            {
                return this.db.MONITOR.Where(x => x.monitorId == id).FirstOrDefault();
            }

            // catch (System.ArgumentNullException)
            catch (Exception e)
            {
                // throw new DatabaseException("There is no record with that ID");
                throw e;
            }
        }

        /// <summary>
        /// Updates a monitors price in the database.
        /// If it doesnt exist then throws an exception.
        /// </summary>
        /// <param name="id">The chosen monitors id.</param>
        /// <param name="price">The new price.</param>
        public void UpdateAr(int id, int price)
        {
            try
            {
                var monitor = this.GetById(id);
                monitor.ar = price;
                this.db.SaveChanges();
            }

            // catch (System.ArgumentNullException)
            catch (Exception e)
            {
                // throw new DatabaseException("There is no record with that ID");
                throw e;
            }
        }

        /// <summary>
        /// Updates a monitors resolution in the database.
        /// If it doesnt exist then throws an exception.
        /// </summary>
        /// <param name="id">The chosen monitors id.</param>
        /// <param name="wh">The new Width and Height.</param>
        public void UpdateFelbontas(int id, string wh)
        {
            try
            {
                var monitor = this.GetById(id);
                monitor.felbontas = wh;
                this.db.SaveChanges();
            }

            // catch (System.ArgumentNullException)
            catch (Exception e)
            {
                // throw new DatabaseException("There is no record with that ID");
                throw e;
            }
        }

        /// <summary>
        /// Updates a monitors screen ratio in the database.
        /// If it doesnt exist then throws an exception.
        /// </summary>
        /// <param name="id">The chosen monitors id.</param>
        /// <param name="wh">The new Width and Height as ratios.</param>
        public void UpdateKeparany(int id, string wh)
        {
            try
            {
                var monitor = this.GetById(id);
                monitor.keparany = wh;
                this.db.SaveChanges();
            }

            // catch (System.ArgumentNullException)
            catch (Exception e)
            {
                // throw new DatabaseException("There is no record with that ID");
                throw e;
            }
        }

        /// <summary>
        /// Updates a monitors diameter in the database.
        /// If it doesnt exist then throws an exception.
        /// </summary>
        /// <param name="id">The chosen monitors id.</param>
        /// <param name="inch">The new diameter.</param>
        public void UpdateKepatlo(int id, int inch)
        {
            try
            {
                var monitor = this.GetById(id);
                monitor.kepatlo = inch;
                this.db.SaveChanges();
            }

            // catch (System.ArgumentNullException)
            catch (Exception e)
            {
                // throw new DatabaseException("There is no record with that ID");
                throw e;
            }
        }

        /// <summary>
        /// Updates a monitors refresh rate in the database.
        /// If it doesnt exist then throws an exception.
        /// </summary>
        /// <param name="id">The chosen monitors id.</param>
        /// <param name="hz">The new refresh rate.</param>
        public void UpdateKepfrissites(int id, int hz)
        {
            try
            {
                var monitor = this.GetById(id);
                monitor.kepfrissites = hz;
                this.db.SaveChanges();
            }

            // catch (System.ArgumentNullException)
            catch (Exception e)
            {
                // throw new DatabaseException("There is no record with that ID");
                throw e;
            }
        }

        /// <summary>
        /// Updates a monitors response time in the database.
        /// If it doesnt exist then throws an exception.
        /// </summary>
        /// <param name="id">The chosen monitors id.</param>
        /// <param name="ms">The new response time.</param>
        public void UpdateValaszido(int id, int ms)
        {
            try
            {
                var monitor = this.GetById(id);
                monitor.valaszido = ms;
                this.db.SaveChanges();
            }

            // catch (System.ArgumentNullException)
            catch (Exception e)
            {
                // throw new DatabaseException("There is no record with that ID");
                throw e;
            }
        }
    }
}
