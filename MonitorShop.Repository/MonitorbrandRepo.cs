﻿// <copyright file="MonitorbrandRepo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MonitorShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using MonitorShop.Data;

    /// <summary>
    /// The monitorbrands repository layer class.
    /// This accesses the database.
    /// </summary>
    public class MonitorbrandRepo : IMonitorbrandRepository
    {
        private MonitorDatabaseEntities db;

        /// <summary>
        /// Initializes a new instance of the <see cref="MonitorbrandRepo"/> class.
        /// </summary>
        /// <param name="db">The database to work on.</param>
        public MonitorbrandRepo(MonitorDatabaseEntities db)
        {
            this.db = db;
        }

        /// <summary>
        /// Inserts a monitorbrand to the database.
        /// </summary>
        /// <param name="monitorbrand">the monitorbrand to be inserted.</param>
        public void CreateMonitorBrand(MONITORBRAND monitorbrand)
        {
            this.db.MONITORBRAND.Add(monitorbrand);
            this.db.SaveChanges();
        }

        /// <summary>
        /// Deletes a monitor from the database.
        /// If it doesnt exist then throws an exception.
        /// </summary>
        /// <param name="id">The chosen monitorbrands id.</param>
        public void DeleteMonitorBrand(int id)
        {
            try
            {
                this.db.MONITORBRAND.Remove(this.GetById(id));
                this.db.SaveChanges();
            }
            catch (System.Data.Entity.Infrastructure.DbUpdateException)
            {
                throw new Exception("Cannot delete record. It is referenced by in another table(s) Delete those first!");
            }
            catch (System.ArgumentNullException)
            {
                throw new Exception("There is no record with that ID");
            }
        }

        /// <summary>
        /// Gets all the monitorbrands from the database.
        /// </summary>
        /// <returns>Returns all the monitorbrand records as a list.</returns>
        public IList<MONITORBRAND> GetAll()
        {
            return this.db.MONITORBRAND.ToList();
        }

        /// <summary>
        /// Gets a monitorbrand by id.
        /// If it doesnt exist then throws an exception.
        /// </summary>
        /// <param name="id">The chosen monitorbrands id.</param>
        /// <returns>Returns the chosen monitorbrand as a class.</returns>
        public MONITORBRAND GetById(int id)
        {
            return this.db.MONITORBRAND.Where(x => x.brandId == id).FirstOrDefault();
        }

        /// <summary>
        /// Updates a monitorbrands foundation year.
        /// If it doesnt exist then throws an exception.
        /// </summary>
        /// <param name="id">The chosen monitorbrands id.</param>
        /// <param name="year">The new year.</param>
        public void UpdateAlapitasEve(int id, int year)
        {
            try
            {
                var monitorbrand = this.GetById(id);
                monitorbrand.alapitasEve = year;
                this.db.SaveChanges();
            }
            catch (System.ArgumentNullException)
            {
                throw new Exception("There is no record with that ID");
            }
        }

        /// <summary>
        /// Updates a monitorbrands name.
        /// If it doesnt exist then throws an exception.
        /// </summary>
        /// <param name="id">The chosen monitorbrands id.</param>
        /// <param name="name">The new name.</param>
        public void UpdateBrandNev(int id, string name)
        {
            try
            {
                var monitorbrand = this.GetById(id);
                monitorbrand.brandNev = name;
                this.db.SaveChanges();
            }
            catch (System.ArgumentNullException)
            {
                throw new Exception("There is no record with that ID");
            }
        }

        /// <summary>
        /// Updates a monitorbrands country.
        /// If it doesnt exist then throws an exception.
        /// </summary>
        /// <param name="id">The chosen monitorbrands id.</param>
        /// <param name="country">The new country.</param>
        public void UpdateOrszag(int id, string country)
        {
            try
            {
                var monitorbrand = this.GetById(id);
                monitorbrand.orszag = country;
                this.db.SaveChanges();
            }
            catch (System.ArgumentNullException)
            {
                throw new Exception("There is no record with that ID");
            }
        }
    }
}
