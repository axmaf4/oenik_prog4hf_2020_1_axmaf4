var interface_monitor_shop_1_1_repository_1_1_i_monitor_repository =
[
    [ "CreateMonitor", "interface_monitor_shop_1_1_repository_1_1_i_monitor_repository.html#a6e50dd3fafaec64f2d74186827a6531e", null ],
    [ "DeleteMonitor", "interface_monitor_shop_1_1_repository_1_1_i_monitor_repository.html#a5c5a74e18a09045a63a022f06622444f", null ],
    [ "GetAll", "interface_monitor_shop_1_1_repository_1_1_i_monitor_repository.html#aec647926e55e0b30fe0eccffdd8f212d", null ],
    [ "GetById", "interface_monitor_shop_1_1_repository_1_1_i_monitor_repository.html#a734ffc96e9afd63140cf5a9f6b96a2fd", null ],
    [ "UpdateAr", "interface_monitor_shop_1_1_repository_1_1_i_monitor_repository.html#a33a131cc37b35b72cdd331b03e426c4c", null ],
    [ "UpdateFelbontas", "interface_monitor_shop_1_1_repository_1_1_i_monitor_repository.html#a29bb450e5515e095109097eaa6cd9e65", null ],
    [ "UpdateKeparany", "interface_monitor_shop_1_1_repository_1_1_i_monitor_repository.html#ac286009c36551ed334c5aac0f591810c", null ],
    [ "UpdateKepatlo", "interface_monitor_shop_1_1_repository_1_1_i_monitor_repository.html#a1423568b011f59cabef1b697a0e5a9b8", null ],
    [ "UpdateKepfrissites", "interface_monitor_shop_1_1_repository_1_1_i_monitor_repository.html#a11cf92f59306dcf8f758df7dc641192f", null ],
    [ "UpdateValaszido", "interface_monitor_shop_1_1_repository_1_1_i_monitor_repository.html#a09d46984d683e341584562bf373bd66b", null ]
];