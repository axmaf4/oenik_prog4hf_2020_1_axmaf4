var class_monitor_shop_1_1_logic_1_1_tests_1_1_logic_test =
[
    [ "AddMonitorBrandTest_Verify", "class_monitor_shop_1_1_logic_1_1_tests_1_1_logic_test.html#ac15c283aa94a6fb730151c407122b615", null ],
    [ "AddMonitorTest_Verify", "class_monitor_shop_1_1_logic_1_1_tests_1_1_logic_test.html#acf073ba0a21a8c6818c87a7df4419d63", null ],
    [ "AddRaktarInfoTest_Verify", "class_monitor_shop_1_1_logic_1_1_tests_1_1_logic_test.html#a2a36d626ee8fb0078d6f7873c91601ff", null ],
    [ "ChangeMonitorPriceTest_Verify", "class_monitor_shop_1_1_logic_1_1_tests_1_1_logic_test.html#a37bc1f0824f58b2afb3c10aea78bd80f", null ],
    [ "ChangeRaktarinfoDarabszamTest_Verify", "class_monitor_shop_1_1_logic_1_1_tests_1_1_logic_test.html#acd75397e636273f305dfec5b5ca2600c", null ],
    [ "ChangeRaktarinfoRendelheto", "class_monitor_shop_1_1_logic_1_1_tests_1_1_logic_test.html#aa6309b3c4cdc20570fa999cbfd677771", null ],
    [ "DeleteMonitorbrandTest_Verify", "class_monitor_shop_1_1_logic_1_1_tests_1_1_logic_test.html#a4d4328410b874aef479a1cc4695c01e8", null ],
    [ "DeleteMonitorTest_Verify", "class_monitor_shop_1_1_logic_1_1_tests_1_1_logic_test.html#a88258bf108e2cd33c4f28c6e1afe3fa3", null ],
    [ "DeleteRaktarinfo", "class_monitor_shop_1_1_logic_1_1_tests_1_1_logic_test.html#affcb31d8401039bd3da85461d4e9b98e", null ],
    [ "GetAllMonitorbrandsTest_Verify", "class_monitor_shop_1_1_logic_1_1_tests_1_1_logic_test.html#acbe81986ab31100838190037cde7ab60", null ],
    [ "Init", "class_monitor_shop_1_1_logic_1_1_tests_1_1_logic_test.html#a391bf70c4bee1bf2400e040194ee86f2", null ]
];