var indexSectionsWithContent =
{
  0: "acdfgijklmnpru",
  1: "ilmpr",
  2: "m",
  3: "acdgijkmru",
  4: "r",
  5: "fp",
  6: "cmn"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "enums",
  5: "enumvalues",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Enumerations",
  5: "Enumerator",
  6: "Pages"
};

