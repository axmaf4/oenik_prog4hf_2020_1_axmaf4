var class_monitor_shop_1_1_repository_1_1_monitor_repo =
[
    [ "MonitorRepo", "class_monitor_shop_1_1_repository_1_1_monitor_repo.html#ae22e10b8816e6ea50a894baeb42b7e57", null ],
    [ "CreateMonitor", "class_monitor_shop_1_1_repository_1_1_monitor_repo.html#ae793c0f8aab2b26f130a99c6dbb77920", null ],
    [ "DeleteMonitor", "class_monitor_shop_1_1_repository_1_1_monitor_repo.html#adf3da2925089ac57d768dc2824fd6a84", null ],
    [ "GetAll", "class_monitor_shop_1_1_repository_1_1_monitor_repo.html#ad52f59b25efeeffb8f4b3c316308eba9", null ],
    [ "GetById", "class_monitor_shop_1_1_repository_1_1_monitor_repo.html#ad3e83658cc24e4bdcf0ec07fce341be1", null ],
    [ "UpdateAr", "class_monitor_shop_1_1_repository_1_1_monitor_repo.html#a46680005a0e20420eea51ff4a5d154d9", null ],
    [ "UpdateFelbontas", "class_monitor_shop_1_1_repository_1_1_monitor_repo.html#a895045fe2198a00ac60b96ad2d8b302d", null ],
    [ "UpdateKeparany", "class_monitor_shop_1_1_repository_1_1_monitor_repo.html#abd938e335f9789712ba7e6c0625441a5", null ],
    [ "UpdateKepatlo", "class_monitor_shop_1_1_repository_1_1_monitor_repo.html#a9a3ecb0b6702c0d26dbd782bfebefc98", null ],
    [ "UpdateKepfrissites", "class_monitor_shop_1_1_repository_1_1_monitor_repo.html#a9306a52bba1a82a5f05298117eadbf66", null ],
    [ "UpdateValaszido", "class_monitor_shop_1_1_repository_1_1_monitor_repo.html#af8e1012647d42d8cfe49d22278ad72c2", null ]
];