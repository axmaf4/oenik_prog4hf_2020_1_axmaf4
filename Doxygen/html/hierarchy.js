var hierarchy =
[
    [ "DbContext", null, [
      [ "MonitorShop.Data::MonitorDatabaseEntities", "class_monitor_shop_1_1_data_1_1_monitor_database_entities.html", null ]
    ] ],
    [ "MonitorShop.Logic.IMonitorLogic", "interface_monitor_shop_1_1_logic_1_1_i_monitor_logic.html", [
      [ "MonitorShop.Logic.MonitorLogic", "class_monitor_shop_1_1_logic_1_1_monitor_logic.html", null ]
    ] ],
    [ "MonitorShop.Repository.IRepository< T >", "interface_monitor_shop_1_1_repository_1_1_i_repository.html", null ],
    [ "MonitorShop.Repository.IRepository< MONITOR >", "interface_monitor_shop_1_1_repository_1_1_i_repository.html", [
      [ "MonitorShop.Repository.IMonitorRepository", "interface_monitor_shop_1_1_repository_1_1_i_monitor_repository.html", [
        [ "MonitorShop.Repository.MonitorRepo", "class_monitor_shop_1_1_repository_1_1_monitor_repo.html", null ]
      ] ]
    ] ],
    [ "MonitorShop.Repository.IRepository< MONITORBRAND >", "interface_monitor_shop_1_1_repository_1_1_i_repository.html", [
      [ "MonitorShop.Repository.IMonitorbrandRepository", "interface_monitor_shop_1_1_repository_1_1_i_monitorbrand_repository.html", [
        [ "MonitorShop.Repository.MonitorbrandRepo", "class_monitor_shop_1_1_repository_1_1_monitorbrand_repo.html", null ]
      ] ]
    ] ],
    [ "MonitorShop.Repository.IRepository< RAKTARINFO >", "interface_monitor_shop_1_1_repository_1_1_i_repository.html", [
      [ "MonitorShop.Repository.IRaktarinfoRepository", "interface_monitor_shop_1_1_repository_1_1_i_raktarinfo_repository.html", [
        [ "MonitorShop.Repository.RaktarinfoRepo", "class_monitor_shop_1_1_repository_1_1_raktarinfo_repo.html", null ]
      ] ]
    ] ],
    [ "MonitorShop.Logic.Tests.LogicTest", "class_monitor_shop_1_1_logic_1_1_tests_1_1_logic_test.html", null ],
    [ "MonitorShop.Data.MONITOR", "class_monitor_shop_1_1_data_1_1_m_o_n_i_t_o_r.html", null ],
    [ "MonitorShop.Data.MONITORBRAND", "class_monitor_shop_1_1_data_1_1_m_o_n_i_t_o_r_b_r_a_n_d.html", null ],
    [ "MonitorShop.Program.Program", "class_monitor_shop_1_1_program_1_1_program.html", null ],
    [ "MonitorShop.Data.RAKTARINFO", "class_monitor_shop_1_1_data_1_1_r_a_k_t_a_r_i_n_f_o.html", null ]
];