var interface_monitor_shop_1_1_logic_1_1_i_monitor_logic =
[
    [ "AddMonitor", "interface_monitor_shop_1_1_logic_1_1_i_monitor_logic.html#af5bb13c1dc2b44631ddba1b28fb0650a", null ],
    [ "AddMonitorBrand", "interface_monitor_shop_1_1_logic_1_1_i_monitor_logic.html#a9bcb316618b4d33e56e1f0094a22683a", null ],
    [ "AddRaktarInfo", "interface_monitor_shop_1_1_logic_1_1_i_monitor_logic.html#ae50e8724c29c61b23bf1f6b494dec482", null ],
    [ "ChangeMonitorPrice", "interface_monitor_shop_1_1_logic_1_1_i_monitor_logic.html#a8df6025c9977d2581fe13b1cacc77fa9", null ],
    [ "ChangeRaktarinfoDarabszam", "interface_monitor_shop_1_1_logic_1_1_i_monitor_logic.html#a1c13f4424e8c2c69de34210e7483f75c", null ],
    [ "ChangeRaktarinfoRendelheto", "interface_monitor_shop_1_1_logic_1_1_i_monitor_logic.html#a193d5c8908419322d229b3b67d38e44e", null ],
    [ "DeleteMonitor", "interface_monitor_shop_1_1_logic_1_1_i_monitor_logic.html#a55c4f99d7ddd9d3033570ce0d9363d17", null ],
    [ "DeleteMonitorbrand", "interface_monitor_shop_1_1_logic_1_1_i_monitor_logic.html#a529ffd75665f79e113989c5cf7c1d242", null ],
    [ "DeleteRaktarinfo", "interface_monitor_shop_1_1_logic_1_1_i_monitor_logic.html#a3d0f2ee004474436dc4e224b1d897f7f", null ],
    [ "GetAllMonitorbrands", "interface_monitor_shop_1_1_logic_1_1_i_monitor_logic.html#a6f01ded284d074a15579b38a74f2d588", null ],
    [ "GetAllMonitors", "interface_monitor_shop_1_1_logic_1_1_i_monitor_logic.html#acde98c864ba11e5b4e1d026e4d2421e1", null ],
    [ "GetAllRaktarinfo", "interface_monitor_shop_1_1_logic_1_1_i_monitor_logic.html#a550e5181c820e1646a7cde283789283a", null ],
    [ "GetCurrentBrandIDs", "interface_monitor_shop_1_1_logic_1_1_i_monitor_logic.html#a298579eea762710e11e03ff158193fc5", null ],
    [ "GetCurrentMonitorIDs", "interface_monitor_shop_1_1_logic_1_1_i_monitor_logic.html#ab0d77c85af7e702e82f4064c506a722d", null ],
    [ "GetGamerMonitorok", "interface_monitor_shop_1_1_logic_1_1_i_monitor_logic.html#a1626b5dd811eaa67d19f2696f91af066", null ],
    [ "GetIrodaiMonitorok", "interface_monitor_shop_1_1_logic_1_1_i_monitor_logic.html#aaf6c76f145234ba83279e0d75b1cf2d1", null ],
    [ "GetKifutoMonitorok", "interface_monitor_shop_1_1_logic_1_1_i_monitor_logic.html#ace38ff23fcfc0bbfc705071ed4b4bb26", null ],
    [ "GetMonitorbrandById", "interface_monitor_shop_1_1_logic_1_1_i_monitor_logic.html#a7efa27da28951859bfa7e381beca5c99", null ],
    [ "GetMonitorById", "interface_monitor_shop_1_1_logic_1_1_i_monitor_logic.html#ad14a7f17be824f36faa633fb919ee9a6", null ],
    [ "GetRaktarinfoById", "interface_monitor_shop_1_1_logic_1_1_i_monitor_logic.html#ad467c2b9d9abba2916191aeecae4a7e3", null ],
    [ "GetRaktaronLevoSzelesMonitorok", "interface_monitor_shop_1_1_logic_1_1_i_monitor_logic.html#a850591b55c76dd1f59fb2ae33e14cffa", null ]
];