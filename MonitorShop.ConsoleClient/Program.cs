﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MonitorShop.ConsoleClient
{
    public class Monitor
    {
        public int ID { get; set; }
        public int BrandID { get; set; }
        public double Diagonal { get; set; }
        public string Ratio { get; set; }
        public string Resolution { get; set; }
        public int RefreshRate { get; set; }
        public int ResponseTime { get; set; }
        public bool Curved { get; set; }
        public int Price { get; set; }

        public override string ToString()
        {
            return $"{ID} - {BrandID} - {Diagonal} - {Ratio} - {Resolution}' - {RefreshRate}hz - {ResponseTime}ms - " + (Curved ? "Hajlitott - " : "Nem hajlitott - ") + Price;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("WAITING...");
            Console.ReadLine();

            string url = "http://localhost:57156/api/MonitorsAPI/";

            using (HttpClient client = new HttpClient())
            {
                string json = client.GetStringAsync(url + "all").Result;
                var list = JsonConvert.DeserializeObject<List<Monitor>>(json);
                
                foreach (var item in list) Console.WriteLine(item);
                Console.ReadLine();

                Dictionary<string, string> postData;
                string response;

                postData = new Dictionary<string, string>();
                postData.Add(nameof(Monitor.ID), "56");
                postData.Add(nameof(Monitor.BrandID), "1");    
                postData.Add(nameof(Monitor.Diagonal), "23");
                postData.Add(nameof(Monitor.Ratio), "16:9");
                postData.Add(nameof(Monitor.Resolution), "1920x1080");
                postData.Add(nameof(Monitor.RefreshRate), "144");
                postData.Add(nameof(Monitor.ResponseTime), "1");
                postData.Add(nameof(Monitor.Curved), "True");
                postData.Add(nameof(Monitor.Price), "80000");

                response = client.PostAsync(url + "add", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("ADD: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();

                int monitorId = JsonConvert.DeserializeObject<List<Monitor>>(json).Single(x => x.ID == 56).ID;
                postData = new Dictionary<string, string>();
                postData.Add(nameof(Monitor.ID), monitorId.ToString());
                postData.Add(nameof(Monitor.BrandID), "1");
                postData.Add(nameof(Monitor.Diagonal), "23");
                postData.Add(nameof(Monitor.Ratio), "16:9");
                postData.Add(nameof(Monitor.Resolution), "1920x1080");
                postData.Add(nameof(Monitor.RefreshRate), "144");
                postData.Add(nameof(Monitor.ResponseTime), "1");
                postData.Add(nameof(Monitor.Curved), "True");
                postData.Add(nameof(Monitor.Price), "60000");

                response = client.PostAsync(url + "mod", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("MOD: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();

                response = client.GetStringAsync(url + "del/" + monitorId).Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("DEL: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();
            }

            Console.ReadLine();
        }
    }
}
