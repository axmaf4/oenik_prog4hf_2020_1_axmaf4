﻿// <copyright file="MonitorLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MonitorShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using MonitorShop.Data;
    using MonitorShop.Repository;

    /// <summary>
    /// Was necessary because of the java end-point.
    /// I needed the raw data from a monitor not just the fancy formatted one.
    /// </summary>
    public enum ReturnFormat
    {
        /// <summary>
        /// returns the monitors properties somewhat edited so it can simply be printed to the console.
        /// </summary>
        FANCY,

        /// <summary>
        /// returns the monitor as value%value%value so it can be used as raw data
        /// </summary>
        PARSABLE,
    }

    /// <summary>
    /// Logic class.
    /// </summary>
    public class MonitorLogic : IMonitorLogic
    {
        private IMonitorRepository monitorRepo;
        private IMonitorbrandRepository monitorbrandRepo;
        private IRaktarinfoRepository raktarinfoRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="MonitorLogic"/> class.
        /// Constructor with no parameters.
        /// </summary>
        public MonitorLogic()
        {
            this.monitorRepo = new MonitorRepo(new MonitorDatabaseEntities());
            this.monitorbrandRepo = new MonitorbrandRepo(new MonitorDatabaseEntities());
            this.raktarinfoRepo = new RaktarinfoRepo(new MonitorDatabaseEntities());
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MonitorLogic"/> class.
        /// Constructor with all parameters.
        /// </summary>
        /// <param name="monitorrepo">the monitor repo.</param>
        /// <param name="monitorbrandrepo">the monitor brand repo.</param>
        /// <param name="raktarinforepo">the raktarinfo repo.</param>
        public MonitorLogic(IMonitorRepository monitorrepo, IMonitorbrandRepository monitorbrandrepo, IRaktarinfoRepository raktarinforepo)
        {
            this.monitorRepo = monitorrepo;
            this.monitorbrandRepo = monitorbrandrepo;
            this.raktarinfoRepo = raktarinforepo;
        }

        /*
public MonitorLogic(IMonitorRepository monitorrepo)
{ monitorRepo = monitorrepo; }
public MonitorLogic(IMonitorbrandRepository monitorbrandrepo)
{ monitorbrandRepo = monitorbrandrepo; }
public MonitorLogic(IRaktarinfoRepository raktarinforepo)
{ raktarinfoRepo = raktarinforepo; }
*/

        /*
         * TODO:
         * fool proofing
         * */
        /*
    /// <summary>
    /// Calls the repository layer to add a monitor.
    /// </summary>
    /// <param name="monitor">the monitor to add.</param>
    public void AddMonitor(MONITOR monitor)
    {
        this.monitorRepo.CreateMonitor(monitor);
    }
    */

        /// <summary>
        /// Calls the repository layer to add a monitor.
        /// </summary>
        /// <param name="id">id of the new monitor.</param>
        /// <param name="brandid">brandid of the new monitor.</param>
        /// <param name="kepatlo">diagonal of the new monitor.</param>
        /// <param name="keparany">screen ratio of the new monitor.</param>
        /// <param name="felbontas">resolution of the new monitor.</param>
        /// <param name="kepfrissites">refresh rate of the new monitor.</param>
        /// <param name="valaszido">response time of the new monitor.</param>
        /// <param name="hajlitott">is the new monitor curved.</param>
        /// <param name="ar">price of the new monitor.</param>
        public void AddMonitor(int id, int brandid, double kepatlo, string keparany, string felbontas, int kepfrissites, int valaszido, bool hajlitott, int ar)
        {
            try
            {
                this.monitorRepo.CreateMonitor(new MONITOR()
                {
                    monitorId = id,
                    brandId = brandid,
                    kepatlo = (decimal)kepatlo,
                    keparany = keparany,
                    felbontas = felbontas,
                    kepfrissites = kepfrissites,
                    valaszido = valaszido,
                    hajlitott = hajlitott,
                    ar = ar,
                });
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Calls the repository layer to add a monitorbrand.
        /// </summary>
        /// <param name="monitorbrand">the monitorbrand to add.</param>
        public void AddMonitorBrand(MONITORBRAND monitorbrand)
        {
            this.monitorbrandRepo.CreateMonitorBrand(monitorbrand);
        }

        /// <summary>
        /// Calls the repository layer to add a raktarinfo.
        /// </summary>
        /// <param name="raktarinfo">the raktarinfo to add.</param>
        public void AddRaktarInfo(RAKTARINFO raktarinfo)
        {
            this.raktarinfoRepo.CreateRaktarinfo(raktarinfo);
        }

        /// <summary>
        /// Calls the repo layer to change the price of a specified monitor.
        /// </summary>
        /// <param name="id">the chosen mointors id.</param>
        /// <param name="newPrice">the nwe price.</param>
        public void ChangeMonitorPrice(int id, int newPrice)
        {
            this.monitorRepo.UpdateAr(id, newPrice);
        }

        /// <summary>
        /// Calls the repo layer to change the darabszam by id.
        /// </summary>
        /// <param name="id">the chosen mointors id.</param>
        /// <param name="newCount">the new count.</param>
        public void ChangeRaktarinfoDarabszam(int id, int newCount)
        {
            this.raktarinfoRepo.UpdateDarabszam(id, newCount);
        }

        /// <summary>
        /// Calls the repo layer to update a raktarinfo by id.
        /// </summary>
        /// <param name="id">the chosen mointors id.</param>
        /// <param name="orderable">the new value.</param>
        public void ChangeRaktarinfoRendelheto(int id, bool orderable)
        {
            this.raktarinfoRepo.UpdateRendelheto(id, orderable);
        }

        /// <summary>
        /// Calls the repo layer to delete a monitor by id.
        /// </summary>
        /// <param name="id">the chosen mointors id.</param>
        public void DeleteMonitor(int id)
        {
            try
            {
                this.monitorRepo.DeleteMonitor(id);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Calls the repo layer to delete a monitorbrand by id.
        /// </summary>
        /// <param name="id">the chosen monitorbrands id.</param>
        public void DeleteMonitorbrand(int id)
        {
            try
            {
                this.monitorbrandRepo.DeleteMonitorBrand(id);
            }
            catch (Exception e)
            {
                throw e;
            }
}

        /// <summary>
        /// Calls the repo layer to delete a raktarinfo.
        /// </summary>
        /// <param name="id">the chosen monitors id.</param>
        public void DeleteRaktarinfo(int id)
        {
            try
            {
                this.raktarinfoRepo.DeleteRaktarinfo(id);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Calls the repo layer to get all the monitorbrand records.
        /// </summary>
        /// <returns>A string that has all the monitorbrands in a formatted way so it is only needed to be printed.</returns>
        public string GetAllMonitorbrands()
        {

            string result = string.Empty;
            foreach (var item in this.monitorbrandRepo.GetAll())
            {
                result += $"{item.brandId} - {item.brandNev} - {item.alapitasEve} - {item.orszag}\n";
            }

            return result;
        }

        /// <summary>
        /// Calls the repo layer to get all the monitor records.
        /// </summary>
        /// <returns>A string that has all the monitors in a formatted way so it is only needed to be printed.</returns>
        public string GetAllMonitors()
        {
            string result = string.Empty;
            foreach (var item in this.monitorRepo.GetAll())
            {
                result += $"{item.monitorId} - {item.brandId} - {item.felbontas} - {item.keparany} - {item.kepatlo}' - {item.valaszido}ms - {item.kepfrissites}hz - " + ((bool)item.hajlitott ? "Hajlitott - " : "Nem hajlitott - ") + item.ar + "\n";
            }

            return result;
        }

        /// <summary>
        /// Calls the repo layer to get a monitorbrand record.
        /// </summary>
        /// <param name="id">The chosem monitors id.</param>
        /// <returns>A string that has the chosen monitorbrand in a formatted way so it is only needed to be printed.</returns>
        public string GetMonitorbrandById(int id)
        {
            var item = this.monitorbrandRepo.GetById(id);
            string result = $"{item.brandId} - {item.brandNev} - {item.alapitasEve} - {item.orszag}\n";

            return result;
        }

        /// <summary>
        /// Calls the repo layer to get a monitor record.
        /// If the returnformat is "fancy" then calls the basic function.
        /// </summary>
        /// <param name="id">The monitors id.</param>
        /// <param name="format">The return format of the generated string.</param>
        /// <returns>This returns the "raw data" format so it can be used at the java end-point.</returns>
        public string GetMonitorById(int id, ReturnFormat format)
        {
            if (format == ReturnFormat.FANCY)
            {
                return this.GetMonitorById(id);
            }

            var item = this.monitorRepo.GetById(id);
            string result = $"{item.monitorId}%{item.brandId}%{item.felbontas}%{item.keparany}%{item.kepatlo}%{item.valaszido}%{item.kepfrissites}%{item.hajlitott}%{item.ar}";

            return result;
        }

        /// <summary>
        /// Calls the repo layer to get a monitor by id.
        /// </summary>
        /// <param name="id">The chosen monitors id.</param>
        /// <returns>Returns the fancy formatted string.</returns>
        public string GetMonitorById(int id)
        {
            var item = this.monitorRepo.GetById(id);
            string result = $"{item.monitorId} - {item.brandId} - {item.felbontas} - {item.keparany} - {item.kepatlo}' - {item.valaszido}ms - {item.kepfrissites}hz - " + ((bool)item.hajlitott ? "Hajlitott - " : "Nem hajlitott - ") + item.ar + "\n";

            return result;
        }

        /// <summary>
        /// Calls the repo layer to get a raktarinfo by id.
        /// </summary>
        /// <param name="id">The chosen monitors id.</param>
        /// <returns>The formatted string of a raktarinfo record.</returns>
        public string GetRaktarinfoById(int id)
        {
            var item = this.raktarinfoRepo.GetById(id);

            string result = $"{item.monitorId} - {item.darabszam}db - " + ((bool)item.rendelheto ? "RENDELHETO" : "NEM RENDELHETO") + "\n";

            return result;
        }

        public MONITOR GetMonitorItemById(int id)
        {
            var item = this.monitorRepo.GetById(id);
            return item;
        }

        public RAKTARINFO GetRaktarinfoItemById(int id)
        {
            var item = this.raktarinfoRepo.GetById(id);
            return item;
        }

        public MONITORBRAND GetMonitorbrandItemById(int id)
        {
            var item = this.monitorbrandRepo.GetById(id);
            return item;
        }

        public IEnumerable<MONITOR> GetAllMonitorItems()
        {
            var item = this.monitorRepo.GetAll();
            return item;
        }

        public IEnumerable<MONITORBRAND> GetAllMonitorBrandItems()
        {
            var item = this.monitorbrandRepo.GetAll();
            return item;
        }

        public IEnumerable<RAKTARINFO> GetAllRaktarinfoItems()
        {
            var item = this.raktarinfoRepo.GetAll();
            return item;
        }

        public void ChangeMonitor(int id, double kepatlo, string keparany, string felbontas, int kepfrissites, int valaszido, bool hajlitott, int ar)
        {
            try
            {
                this.monitorRepo.UpdateKepatlo(id, (int)kepatlo);
                this.monitorRepo.UpdateKeparany(id, keparany);
                this.monitorRepo.UpdateFelbontas(id, felbontas);
                this.monitorRepo.UpdateKepfrissites(id, kepfrissites);
                this.monitorRepo.UpdateValaszido(id, valaszido);
                this.monitorRepo.UpdateAr(id, ar);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Calls the repo layer to get all the raktarinfo records.
        /// </summary>
        /// <returns>All the raktarinfo records as a formatted string.</returns>
        public string GetAllRaktarinfo()
        {
            string result = string.Empty;

            foreach (var item in this.raktarinfoRepo.GetAll())
            {
                result += $"{item.monitorId} - {item.darabszam}db - " + ((bool)item.rendelheto ? "RENDELHETO\n" : "NEM RENDELHETO\n");
            }

            return result;
        }

        /// <summary>
        /// Calls the repo layer to get all the monitors and filters for "gamer" properties.
        /// </summary>
        /// <returns>The formatted string of all the filtered records.</returns>
        public string GetGamerMonitorok()
        {
            var allItems = this.monitorRepo.GetAll();
            string result = string.Empty;

            var q = from item in allItems
                    where item.valaszido == 1 && item.kepfrissites >= 120 && item.kepatlo > 23
                    select item;

            if (q is null)
            {
                return "Jelenleg nincs monitor ilyen kriteriummal";
            }

            foreach (var monitor in q)
            {
                result += $"{monitor.monitorId} - {monitor.brandId} - {monitor.felbontas} - {monitor.keparany} - {monitor.kepatlo}' - {monitor.valaszido}ms - {monitor.kepfrissites}hz - " + ((bool)monitor.hajlitott ? "Hajlitott - " : "Nem hajlitott - ") + monitor.ar + "\n";
            }

            return result;
        }

        /// <summary>
        /// Calls the repo layer to get all the monitors and filters for "office" properties.
        /// </summary>
        /// <returns>The formatted string of all the filtered records.</returns>
        public string GetIrodaiMonitorok()
        {
            var allItems = this.monitorRepo.GetAll();
            string result = string.Empty;

            var q = from item in allItems
                    where item.felbontas == "1920x1080" && (bool)item.hajlitott && item.kepatlo > 22
                    select item;

            if (q is null)
            {
                return "Jelenleg nincs monitor ilyen kriteriummal";
            }

            foreach (var monitor in q)
            {
                result += $"{monitor.monitorId} - {monitor.brandId} - {monitor.felbontas} - {monitor.keparany} - {monitor.kepatlo}' - {monitor.valaszido}ms - {monitor.kepfrissites}hz - " + ((bool)monitor.hajlitott ? "Hajlitott - " : "Nem hajlitott - ") + monitor.ar + "\n";
            }

            return result;
        }

        /// <summary>
        /// Calls the repo layer to get all the monitors and filters for "last piece" properties.
        /// </summary>
        /// <returns>The formatted string of all the filtered records.</returns>
        public string GetKifutoMonitorok()
        {
            var allMonitors = this.monitorRepo.GetAll();
            var allRaktarinfos = this.raktarinfoRepo.GetAll();

            string result = string.Empty;

            var q = from monitor in allMonitors
                    join raktarinfo in allRaktarinfos on monitor.monitorId equals raktarinfo.monitorId
                    where raktarinfo.darabszam > 0 && raktarinfo.rendelheto == false
                    select monitor;

            if (q is null)
            {
                return "Jelenleg nincs monitor ilyen kriteriummal";
            }

            foreach (var monitor in q)
            {
                result += $"{monitor.monitorId} - {monitor.brandId} - {monitor.felbontas} - {monitor.keparany} - {monitor.kepatlo}' - {monitor.valaszido}ms - {monitor.kepfrissites}hz - " + ((bool)monitor.hajlitott ? "Hajlitott - " : "Nem hajlitott - ") + monitor.ar + "\n";
            }

            return result;
        }

        /// <summary>
        /// Calls the repo layer to get all the monitors and filters for "available and wide-screen" properties.
        /// </summary>
        /// <returns>The formatted string of all the filtered records.</returns>
        public string GetRaktaronLevoSzelesMonitorok()
        {
            var allMonitors = this.monitorRepo.GetAll();
            var allRaktarinfos = this.raktarinfoRepo.GetAll();

            string result = string.Empty;

            var q = from monitor in allMonitors
                    join raktarinfo in allRaktarinfos on monitor.monitorId equals raktarinfo.monitorId
                    where raktarinfo.darabszam > 0 && monitor.keparany == "21:9"
                    select monitor;

            if (q is null)
            {
                return "Jelenleg nincs monitor ilyen kriteriummal";
            }

            foreach (var monitor in q)
            {
                result += $"{monitor.monitorId} - {monitor.brandId} - {monitor.felbontas} - {monitor.keparany} - {monitor.kepatlo}' - {monitor.valaszido}ms - {monitor.kepfrissites}hz - " + ((bool)monitor.hajlitott ? "Hajlitott - " : "Nem hajlitott - ") + monitor.ar + "\n";
            }

            return result;
        }

        /// <summary>
        /// Calls the repo layer to get all the monitors and collects its ids.
        /// </summary>
        /// <returns>The formatted string of all current monitor ids.</returns>
        public string GetCurrentMonitorIDs()
        {
            string result = string.Empty;

            foreach (var item in this.monitorRepo.GetAll())
            {
                result += item.monitorId + ";";
            }

            return result.Remove(result.Length - 1);
        }

        /// <summary>
        /// Calls the repo layer to get all the monitors and collects its ids.
        /// </summary>
        /// <returns>The formatted string of all current mointorbrand ids.</returns>
        public string GetCurrentBrandIDs()
        {
            string result = string.Empty;

            foreach (var item in this.monitorbrandRepo.GetAll())
            {
                result += item.brandId + ";";
            }

            return result.Remove(result.Length - 1);
        }
        
    }
}
