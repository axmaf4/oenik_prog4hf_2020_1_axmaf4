﻿// <copyright file="IMonitorLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MonitorShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using MonitorShop.Data;
    using MonitorShop.Repository;

    /// <summary>
    /// The interface of the logic layer.
    /// </summary>
    public interface IMonitorLogic
    {
        /// <summary>
        /// Calls the repo layer to get all the monitors and collects its ids.
        /// </summary>
        /// <returns>The formatted string of all current monitor ids.</returns>
        string GetCurrentMonitorIDs();

        /// <summary>
        /// Calls the repo layer to get all the monitors and collects its ids.
        /// </summary>
        /// <returns>The formatted string of all current mointorbrand ids.</returns>
        string GetCurrentBrandIDs();

        /// <summary>
        /// Calls the repository layer to add a monitor.
        /// </summary>
        /// <param name="id">id of the new monitor.</param>
        /// <param name="brandid">brandid of the new monitor.</param>
        /// <param name="kepatlo">diagonal of the new monitor.</param>
        /// <param name="keparany">screen ratio of the new monitor.</param>
        /// <param name="felbontas">resolution of the new monitor.</param>
        /// <param name="kepfrissites">refresh rate of the new monitor.</param>
        /// <param name="valaszido">response time of the new monitor.</param>
        /// <param name="hajlitott">is the new monitor curved.</param>
        /// <param name="ar">price of the new monitor.</param>
        void AddMonitor(int id, int brandid, double kepatlo, string keparany, string felbontas, int kepfrissites, int valaszido, bool hajlitott, int ar);
        /*
        /// <summary>
        /// Calls the repository layer to add a monitor.
        /// </summary>
        /// <param name="monitor">the monitor to add.</param>
        // void AddMonitor(MONITOR monitor);
        */

        /// <summary>
        /// Calls the repository layer to add a monitorbrand.
        /// </summary>
        /// <param name="monitorbrand">the monitorbrand to add.</param>
        void AddMonitorBrand(MONITORBRAND monitorbrand);

        /// <summary>
        /// Calls the repository layer to add a raktarinfo.
        /// </summary>
        /// <param name="raktarinfo">the raktarinfo to add.</param>
        void AddRaktarInfo(RAKTARINFO raktarinfo);

        /// <summary>
        /// Calls the repo layer to get a monitor record.
        /// If the returnformat is "fancy" then calls the basic function.
        /// </summary>
        /// <param name="id">The monitors id.</param>
        /// <param name="format">The return format of the generated string.</param>
        /// <returns>This returns the "raw data" format so it can be used at the java end-point.</returns>
        string GetMonitorById(int id, ReturnFormat format);

        /// <summary>
        /// Calls the repo layer to get a monitorbrand record.
        /// </summary>
        /// <param name="id">The chosem monitors id.</param>
        /// <returns>A string that has the chosen monitorbrand in a formatted way so it is only needed to be printed.</returns>
        string GetMonitorbrandById(int id);

        /// <summary>
        /// Calls the repo layer to get a raktarinfo by id.
        /// </summary>
        /// <param name="id">The chosen monitors id.</param>
        /// <returns>The formatted string of a raktarinfo record.</returns>
        string GetRaktarinfoById(int id);

        /// <summary>
        /// Calls the repo layer to get all the monitor records.
        /// </summary>
        /// <returns>A string that has all the monitors in a formatted way so it is only needed to be printed.</returns>
        string GetAllMonitors();

        /// <summary>
        /// Calls the repo layer to get all the monitorbrand records.
        /// </summary>
        /// <returns>A string that has all the monitorbrands in a formatted way so it is only needed to be printed.</returns>
        string GetAllMonitorbrands();

        /// <summary>
        /// Calls the repo layer to get all the raktarinfo records.
        /// </summary>
        /// <returns>All the raktarinfo records as a formatted string.</returns>
        string GetAllRaktarinfo();

        /// <summary>
        /// Calls the repo layer to change the price of a specified monitor.
        /// </summary>
        /// <param name="id">the chosen mointors id.</param>
        /// <param name="newPrice">the nwe price.</param>
        void ChangeMonitorPrice(int id, int newPrice);

        /// <summary>
        /// Calls the repo layer to change the darabszam by id.
        /// </summary>
        /// <param name="id">the chosen mointors id.</param>
        /// <param name="newCount">the new count.</param>
        void ChangeRaktarinfoDarabszam(int id, int newCount);

        /// <summary>
        /// Calls the repo layer to update a raktarinfo by id.
        /// </summary>
        /// <param name="id">the chosen mointors id.</param>
        /// <param name="orderable">the new value.</param>
        void ChangeRaktarinfoRendelheto(int id, bool orderable);

        /// <summary>
        /// Calls the repo layer to delete a monitor by id.
        /// </summary>
        /// <param name="id">the chosen mointors id.</param>
        void DeleteMonitor(int id);

        /// <summary>
        /// Calls the repo layer to delete a monitorbrand by id.
        /// </summary>
        /// <param name="id">the chosen monitorbrands id.</param>
        void DeleteMonitorbrand(int id);

        /// <summary>
        /// Calls the repo layer to delete a raktarinfo.
        /// </summary>
        /// <param name="id">the chosen monitors id.</param>
        void DeleteRaktarinfo(int id);

        /// <summary>
        /// Calls the repo layer to get all the monitors and filters for "last piece" properties.
        /// </summary>
        /// <returns>The formatted string of all the filtered records.</returns>
        string GetKifutoMonitorok();

        /// <summary>
        /// Calls the repo layer to get all the monitors and filters for "gamer" properties.
        /// </summary>
        /// <returns>The formatted string of all the filtered records.</returns>
        string GetGamerMonitorok();

        /// <summary>
        /// Calls the repo layer to get all the monitors and filters for "office" properties.
        /// </summary>
        /// <returns>The formatted string of all the filtered records.</returns>
        string GetIrodaiMonitorok();

        /// <summary>
        /// Calls the repo layer to get all the monitors and filters for "available and wide-screen" properties.
        /// </summary>
        /// <returns>The formatted string of all the filtered records.</returns>
        string GetRaktaronLevoSzelesMonitorok();



        MONITOR GetMonitorItemById(int id);

        RAKTARINFO GetRaktarinfoItemById(int id);

        MONITORBRAND GetMonitorbrandItemById(int id);

        IEnumerable<MONITOR> GetAllMonitorItems();

        IEnumerable<MONITORBRAND> GetAllMonitorBrandItems();

        IEnumerable<RAKTARINFO> GetAllRaktarinfoItems();

        void ChangeMonitor(int id, double kepatlo, string keparany, string felbontas, int kepfrissites, int valaszido, bool hajlitott, int ar);
    }
}
