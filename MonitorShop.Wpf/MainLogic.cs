﻿using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;

namespace MonitorShop.Wpf
{
    class MainLogic
    {
        string url = "http://localhost:57156/api/MonitorsAPI/";
        HttpClient client = new HttpClient();

        void SendMessage(bool success)
        {
            string message = (success ? "Operation completed successfully" : "Operation failed");
            Messenger.Default.Send(message, "CarResult");
        }

        public List<MonitorVM> ApiGetMonitors()
        {
            string json = client.GetStringAsync(this.url + "all").Result;
            var list = JsonConvert.DeserializeObject<List<MonitorVM>>(json);
            //SendMessage(true);
            return list;
        }

        public void ApiDelMonitor(MonitorVM monitor)
        {
            bool success = false;
            if (monitor != null)
            {
                string json = client.GetStringAsync(url + "del/" + monitor.Id).Result;
                JObject obj = JObject.Parse(json);
                success = (bool)obj["OperationResult"];
            }
            SendMessage(success);
        }

        bool ApiEditMonitor(MonitorVM monitor, bool isEditing)
        {
            if (monitor == null) return false;
            string tmpUrl = (isEditing ? this.url + "mod" : this.url + "add");

            Dictionary<string, string> postData = new Dictionary<string, string>();

            postData.Add(nameof(MonitorVM.Id), monitor.Id.ToString());
            postData.Add(nameof(MonitorVM.BrandId), monitor.BrandId.ToString());
            postData.Add(nameof(MonitorVM.Diagonal), monitor.Diagonal.ToString());
            postData.Add(nameof(MonitorVM.Ratio), monitor.Ratio);
            postData.Add(nameof(MonitorVM.Resolution), monitor.Resolution);
            postData.Add(nameof(MonitorVM.RefreshRate), monitor.RefreshRate.ToString());
            postData.Add(nameof(MonitorVM.ResponseTime), monitor.ResponseTime.ToString());
            postData.Add(nameof(MonitorVM.Curved), monitor.Curved.ToString());
            postData.Add(nameof(MonitorVM.Price), monitor.Price.ToString());

            string json = client.PostAsync(tmpUrl, new FormUrlEncodedContent(postData))
                            .Result.Content.ReadAsStringAsync().Result;

            JObject obj = JObject.Parse(json);
            return (bool)obj["OperationResult"];
        }

        public void EditMonitor(MonitorVM monitor, Func<MonitorVM, bool> editor)
        {
            MonitorVM clone = new MonitorVM();
            if (monitor != null) clone.CopyFrom(monitor);

            bool? success = editor?.Invoke(clone);

            if (success == true)
            {
                if (monitor != null) success = ApiEditMonitor(clone, true);
                else success = ApiEditMonitor(clone, false);
            }
            SendMessage(success == true);
        }

    }
}
