﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MonitorShop.Wpf
{
    class MainVM : ViewModelBase
    {
        private MainLogic logic;
		private MonitorVM selectedMonitor;
		private ObservableCollection<MonitorVM> allMonitors;

		public ObservableCollection<MonitorVM> AllMonitors
		{
			get { return allMonitors; }
			set { Set(ref allMonitors, value); }
		}

		public MonitorVM SelectedMonitor
		{
			get { return selectedMonitor; }
			set { Set(ref selectedMonitor, value); }
		}

		public ICommand AddCmd { get; private set; }
		public ICommand DelCmd { get; private set; }
		public ICommand ModCmd { get; private set; }
		public ICommand LoadCmd { get; private set; }

		public Func<MonitorVM, bool> EditorFunc { get; set; }
		
		public MainVM()
		{
			logic = new MainLogic();

			DelCmd = new RelayCommand(() => logic.ApiDelMonitor(selectedMonitor));
			AddCmd = new RelayCommand(() => logic.EditMonitor(null, EditorFunc));
			ModCmd = new RelayCommand(() => logic.EditMonitor(selectedMonitor, EditorFunc));
			LoadCmd = new RelayCommand(
				() => AllMonitors = new ObservableCollection<MonitorVM>(logic.ApiGetMonitors()));
		}
	}
}
