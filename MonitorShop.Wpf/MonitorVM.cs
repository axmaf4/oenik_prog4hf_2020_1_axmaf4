﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonitorShop.Wpf
{
    class MonitorVM : ObservableObject
    {
		private int id;
		private int brandId;
		private double diagonal;
		private string ratio;
		private string resolution;
		private int refreshRate;
		private int responseTime;
		private bool curved;
		private int price;

		public int Price
		{
			get { return price; }
			set { Set(ref price, value); }
		}

		public bool Curved
		{
			get { return curved; }
			set { Set(ref curved, value); }
		}

		public int ResponseTime
		{
			get { return responseTime; }
			set { Set(ref responseTime, value); }
		}

		public int RefreshRate
		{
			get { return refreshRate; }
			set { Set(ref refreshRate, value); }
		}

		public string Resolution
		{
			get { return resolution; }
			set { Set(ref resolution, value); }
		}

		public string Ratio
		{
			get { return ratio; }
			set { Set(ref ratio, value); }
		}

		public double Diagonal
		{
			get { return diagonal; }
			set { Set(ref diagonal, value); }
		}


		public int BrandId
		{
			get { return brandId; }
			set { Set(ref brandId, value); }
		}

		public int Id
		{
			get { return id; }
			set { Set(ref id, value); }
		}

		public void CopyFrom(MonitorVM other)
		{
			if (other == null) return;

			this.Id = other.id;
			this.BrandId = other.brandId;
			this.Diagonal = other.diagonal;
			this.Ratio = other.ratio;
			this.Resolution = other.resolution;
			this.RefreshRate = other.refreshRate;
			this.ResponseTime = other.responseTime;
			this.Curved = other.curved;
			this.Price = other.price;
		}
	}
}
