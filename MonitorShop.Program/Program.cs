﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MonitorShop.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Xml.Linq;
    using MonitorShop.Data;
    using MonitorShop.Logic;

    /// <summary>
    /// The main program class.
    /// </summary>
    public class Program
    {
        private static MonitorLogic logic = new MonitorLogic();
        private static Random rand = new Random();

        /// <summary>
        /// Gets the given inputs from the user. Turned out to be really handy with the simpler menu format.
        /// </summary>
        /// <param name="message">The general message the user will see when asking for input.</param>
        /// <param name="arguments">Specifies the input parameters .Lenght is the number of inputs and the strings inside are the specifications for the currently asked input.</param>
        /// <param name="ask"> If true asks the user if he really wants to do the action. usually at deletion, updating or creating. </param>
        /// <returns> The input as a string array which can be parsed accordingly.</returns>
        public static string[] GetInput(string message, string[] arguments, bool ask)
        {
            Console.Clear();
            if (ask)
            {
                Console.Write($"Are you sure you want to do the following action: {message} ? \n [type no if you want to go back] >");
                string inp = Console.ReadLine();
                if (inp.ToLower() == "no")
                {
                    return "no%".Split('%');
                }
            }
            Console.Clear();
            Console.WriteLine("Getting input for: " + message + "\n");

            string result = string.Empty;
            string tmp = string.Empty;

            for (int i = 0; i < arguments.Length; i++)
            {
                Console.Write(arguments[i] + "> ");

                tmp = string.Empty;
                while (tmp == string.Empty)
                {
                    tmp = Console.ReadLine();
                }

                result += tmp;
                if (i != arguments.Length - 1)
                {
                    result += "%";
                }
            }

            return result.Split('%');
        }

        /// <summary>
        /// Decides which MONITOR crud function to run based on the input character. Also gets the inputs.
        /// </summary>
        /// <param name="crud">The input character chosen by the user.</param>
        public static void Monitorcrud(char crud)
        {
            string[] input;
            switch (char.ToLower(crud))
            {
                case 'c':
                    input = GetInput(
                        $"Creating new monitor",
                        new string[] { $"\nexisting monitor ids:{logic.GetCurrentMonitorIDs()} so dont use these\n\nmonitor ID", 
                            $"existing brand ids:{logic.GetCurrentBrandIDs()} so pick one of these\n\nBrand ID", 
                            "Kepatlo", 
                            "Keparany([NUMBER]:[NUMBER])", 
                            "Felbontas([NUMBER]x[NUMBER])", 
                            "Kepfrissites", 
                            "Valaszido", 
                            "Hajlitott([True/False])", 
                            "Ar", },
                        true);

                    if (input[0].ToLower() == "no")
                    {
                        break;
                    }

                    logic.AddMonitor(int.Parse(input[0]), int.Parse(input[1]), double.Parse(input[2]), input[3], input[4], int.Parse(input[5]), int.Parse(input[6]), bool.Parse(input[7]), int.Parse(input[8]));

                    break;
                case 'r':
                    input = GetInput($"Getting a monitor\ncurrent ids:{logic.GetCurrentMonitorIDs()}", new string[] { "monitor ID(-1 if you want all)", }, false);

                    if (int.Parse(input[0]) == -1)
                    {
                        Console.WriteLine(logic.GetAllMonitors());
                    }
                    else
                    {
                        try
                        {
                            Console.WriteLine(logic.GetMonitorById(int.Parse(input[0]), ReturnFormat.FANCY));
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                    }

                    break;
                case 'u':
                    input = GetInput($"Updating monitor price\n", new string[] { $"current ids:{logic.GetCurrentMonitorIDs()}\n\nmonitor ID", "Uj ar" }, true);

                    if (input[0].ToLower() == "no")
                    {
                        break;
                    }

                    if (input[0].ToLower() == "no")
                    {
                        break;
                    }

                    try
                    {
                        logic.ChangeMonitorPrice(int.Parse(input[0]), int.Parse(input[1]));
                        Console.WriteLine("monitor price updated");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }

                    break;
                case 'd':
                    Raktarinfocrud('d');

                    break;
                default:
                    Console.WriteLine("ROSSZ OPCIO");
                    break;
            }
        }

        /// <summary>
        /// Decides which MONITORBRAND crud function to run based on the input character. Also gets the inputs.
        /// </summary>
        /// <param name="crud">The input character chosen by the user.</param>
        public static void MonitorBrandcrud(char crud)
        {
            string[] input;
            switch (char.ToLower(crud))
            {
                case 'c':
                    input = GetInput($"Creating new monitor brand \n", new string[] { $"current ids:{logic.GetCurrentBrandIDs()}\n\nbrand ID", "brand nev", "Orszag", "alapitas eve" }, true);

                    if (input[0].ToLower() == "no")
                    {
                        break;
                    }

                    logic.AddMonitorBrand(new MONITORBRAND()
                    {
                        brandId = int.Parse(input[0]),
                        brandNev = input[1],
                        orszag = input[2],
                        alapitasEve = int.Parse(input[3]),
                    });

                    break;
                case 'r':
                    input = GetInput($"Getting a monitor brand \ncurrent ids:{logic.GetCurrentBrandIDs()}", new string[] { "monitor brand ID(-1 if you want all)" }, false);
                    if (int.Parse(input[0]) == -1)
                    {
                        Console.WriteLine(logic.GetAllMonitorbrands());
                    }
                    else
                    {
                        try
                        {
                            Console.WriteLine(logic.GetMonitorbrandById(int.Parse(input[0])));
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                    }

                    break;
                case 'u':
                    Console.WriteLine("cant update a brand!");
                    break;
                case 'd':

                    input = GetInput($"Deleting a brand {logic.GetCurrentBrandIDs()}", new string[] { "monitor brand ID" }, true);

                    if (input[0].ToLower() == "no")
                    {
                        break;
                    }

                    try
                    {
                        logic.DeleteMonitorbrand(int.Parse(input[0]));
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }

                    break;
                default:
                    Console.WriteLine("wrong option");
                    break;
            }
        }

        /// <summary>
        /// Decides which RAKTARINFO crud function to run based on the input character. Also gets the inputs.
        /// </summary>
        /// <param name="crud">The input character chosen by the user.</param>
        public static void Raktarinfocrud(char crud)
        {
            string[] input;

            switch (char.ToLower(crud))
            {
                case 'c':
                    input = GetInput($"Creating a new raktarinfo\n", new string[] { $"Current monitorIDs: {logic.GetCurrentMonitorIDs()}\n\nmonitor ID", "darabszam", "Rendelheto([True / False])" }, true);

                    if (input[0].ToLower() == "no")
                    {
                        break;
                    }

                    logic.AddRaktarInfo(new RAKTARINFO()
                    {
                        monitorId = int.Parse(input[0]),
                        darabszam = int.Parse(input[1]),
                        rendelheto = bool.Parse(input[2]),
                    });

                    break;
                case 'r':

                    input = GetInput($"Getting a raktarinfo\n", new string[] { $"Current monitorIDs: {logic.GetCurrentMonitorIDs()}\n\nmonitor ID(-1 if you want all)" }, false);

                    if (input[0].ToLower() == "no")
                    {
                        break;
                    }

                    if (int.Parse(input[0]) == -1)
                    {
                        Console.WriteLine(logic.GetAllRaktarinfo());
                    }
                    else
                    {
                        try
                        {
                            Console.WriteLine(logic.GetRaktarinfoById(int.Parse(input[0])));
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                    }

                    break;
                case 'u':
                    input = GetInput($"updating a raktarinfo\n", new string[] { $"currend monitorIDs: {logic.GetCurrentMonitorIDs()}\n\nmonitor ID", "uj darabszam", "uj rendelhetoseg([True/False])" }, true);

                    if (input[0].ToLower() == "no")
                    {
                        break;
                    }

                    try
                    {
                        logic.ChangeRaktarinfoDarabszam(int.Parse(input[0]), int.Parse(input[1]));
                        Console.WriteLine("Changed chosen raktarinfos sarabszam");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }

                    try
                    {
                        logic.ChangeRaktarinfoRendelheto(int.Parse(input[0]), bool.Parse(input[2]));
                        Console.WriteLine("Changed chosen raktarinfos 'rendelhetoseg'");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }

                    break;
                case 'd':
                    input = GetInput($"Deleting a raktarinfo.. !!! You can only delete a Monitor or Raktarinfo if you delete both.!!! \n", new string[] { $"current monitorIDs: {logic.GetCurrentMonitorIDs()}\n\nType in a monitorID, or \"no/No/NO\" if you want to go back" }, true);

                    if (input[0].ToLower() == "no")
                    {
                        break;
                    }
                    else
                    {
                        try
                        {
                            logic.DeleteRaktarinfo(int.Parse(input[0]));
                            logic.DeleteMonitor(int.Parse(input[0]));
                            Console.WriteLine("Deleted chosen monitor with its raktarinfo");
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                    }

                    break;

                default:
                    Console.WriteLine("Wrong option");
                    break;
            }
        }

        /// <summary>
        /// One of the non-crud functions.
        /// </summary>
        public static void KifutoMonitorokListazasa()
        {
            Console.WriteLine(logic.GetKifutoMonitorok());
        }

        /// <summary>
        /// One of the non-crud functions.
        /// </summary>
        public static void GamerMonitorokListazasa()
        {
            Console.WriteLine(logic.GetGamerMonitorok());
        }

        /// <summary>
        /// One of the non-crud functions.
        /// </summary>
        public static void RaktaronLevoMonitorokListazasa()
        {
            Console.WriteLine(logic.GetRaktaronLevoSzelesMonitorok());
        }

        /// <summary>
        /// The java end-point interaction.
        /// </summary>
        public static void JavaInterakcio()
        {
            WebClient javaClient = new WebClient();
            string[] monitorok = logic.GetCurrentMonitorIDs().Split(';');
            int selectedId = int.Parse(monitorok[rand.Next(monitorok.Length - 1)]);
            string[] selectedMonitorParams = logic.GetMonitorById(selectedId, ReturnFormat.PARSABLE).Split('%');
            string[] paramNames = new string[] { "monitorId", "brandId", "kepatlo", "keparany", "felbontas", "kepfrissites", "valaszido", "hajlitott", "ar" };

            for (int i = 0; i < selectedMonitorParams.Length; i++)
            {
                javaClient.QueryString.Add(paramNames[i], selectedMonitorParams[i]);
            }

            try
            {
                string result = javaClient.DownloadString("http://localhost:8080/MonitorShop.JavaWeb/Learazas");

                XDocument learazott = XDocument.Parse(result);
                var q = learazott.Root;

                for (int i = 0; i < paramNames.Length; i++)
                {
                    Console.Write($"{paramNames[i]}:{q.Element(paramNames[i]).Value.Replace('\n', char.MinValue)}; ");
                }
            }
            catch (System.Net.WebException)
            {
                Console.WriteLine("\nJava End-point is not running!");
            }
        }

        /// <summary>
        /// The hearth of the menu.
        /// Splits the input parameter into two.
        /// Decides which specified function to call.
        /// </summary>
        /// <param name="answer">The raw user input which will then be split into two pars.</param>
        /// <returns> A boolean because this determines whether the menu should run again or not. </returns>
        public static bool MenuDecide(string answer)
        {
            string[] tmp = answer.Split(' ');

            int a = int.Parse(tmp[0]);

            if (tmp.Length > 1)
            {
                char b = tmp[1][0];
                switch (a)
                {
                    case 1:
                        Monitorcrud(b);
                        break;
                    case 2:
                        MonitorBrandcrud(b);
                        break;
                    case 3:
                        Raktarinfocrud(b);
                        break;
                    default:
                        throw new System.FormatException();

                }
            }

            switch (a)
            {
                case 4:
                    KifutoMonitorokListazasa();
                    break;
                case 5:
                    GamerMonitorokListazasa();
                    break;
                case 6:
                    RaktaronLevoMonitorokListazasa();
                    break;
                case 7:
                    JavaInterakcio();
                    break;
                case 8:
                    return false;
                default:
                    //throw new System.FormatException();
                    break;
            }

            return true;
        }

        /// <summary>
        /// The second(?) hearth of the menu.
        /// it has the while loop.
        /// </summary>
        /// <param name="args"> The automatically generated argument. </param>
        public static void Main(string[] args)
        {
            bool run = true;

            while (run)
            {
                Console.Write("====MENU====\n" +
                                    "[1]Monitor\n" +
                                    "[2]MonitorBrand\n" +
                                    "[3]RaktarInfo\n" +
                                    "  c/r/u/d\n" +
                                    " (create/read/update/delete)\n" +
                                    "[4]KifutoMonitorok\n" +
                                    "[5]GamerMonitorok\n" +
                                    "[6]Rogton Szallithato monitorok\n" +
                                    "[7]Java Interakcio\n" +
                                    "[8]Exit\n\n" +
                                    "examples: '1 c', '4'\n\n>>>");

                string answer = Console.ReadLine();

                try
                {
                    run = MenuDecide(answer);
                }
                catch (System.FormatException)
                {
                    Console.WriteLine("Wrong input format! Either '[NUMBER][SPACE][c/r/u/d]' or '[NUMBER]'!");
                }

                Console.WriteLine("\n----------------------------------\n");
            }
        }
    }
}
