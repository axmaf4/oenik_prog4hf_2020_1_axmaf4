﻿IF OBJECT_ID('RAKTARINFO', 'U') IS NOT NULL DROP TABLE RAKTARINFO;
IF OBJECT_ID('MONITOR', 'U') IS NOT NULL DROP TABLE MONITOR;
IF OBJECT_ID('MONITORBRAND', 'U') IS NOT NULL DROP TABLE MONITORBRAND;



CREATE TABLE MONITORBRAND(
	brandId				NUMERIC(2) NOT NULL,
    brandNev            VARCHAR(14),
    alapitasEve         NUMERIC(4),
	orszag				VARCHAR(15),
CONSTRAINT MONITORBRAND_pK PRIMARY KEY (brandId)
);
INSERT INTO MONITORBRAND VALUES (1, 'ACEL', 1990, 'HUNGARY');
INSERT INTO MONITORBRAND VALUES (2, 'ISUS', 2005, 'CHINA');
INSERT INTO MONITORBRAND VALUES (3, 'ELGE', 2005, 'GERMANY');
INSERT INTO MONITORBRAND VALUES (4, 'TONY', 2005, 'NETHERLANDS');
INSERT INTO MONITORBRAND VALUES (5, 'BENKO', 2010, 'HUNGARY');

CREATE TABLE MONITOR(
	monitorId			NUMERIC(4) NOT NULL,
	brandId				NUMERIC(2) NOT NULL,
	kepatlo				NUMERIC(3,1),
	keparany			VARCHAR(5),
	felbontas			VARCHAR(12),
	kepfrissites		NUMERIC(5),
	valaszido			NUMERIC(2),
	hajlitott			BIT,
	ar					NUMERIC(10),
CONSTRAINT MONITOR_PK PRIMARY KEY (monitorId),
CONSTRAINT MONITOR_FK FOREIGN KEY (brandId) REFERENCES MONITORBRAND (brandId),
CONSTRAINT KEPARANY_CHK CHECK (keparany like '%:%'),
CONSTRAINT FELBONTAS_CHK CHECK (felbontas like '%x%')
);
INSERT INTO MONITOR VALUES (1, 1, 24, '16:9', '1920x1080', 144, 1, 0,  123000);
INSERT INTO MONITOR VALUES (2, 4, 27, '16:9', '1920x1080', 60, 5, 0,   50000);
INSERT INTO MONITOR VALUES (3, 3, 29, '21:9', '2560x1080', 60, 5, 1,   73000);
INSERT INTO MONITOR VALUES (13, 3, 29, '21:9', '2560x1080', 144, 1, 1, 176000);
INSERT INTO MONITOR VALUES (12, 1, 24, '16:9', '2560x1440', 90, 3, 0,  137000);
INSERT INTO MONITOR VALUES (14, 2, 24, '16:9', '2560x1440', 144, 1, 0, 150000);
INSERT INTO MONITOR VALUES (10, 4, 27, '16:9', '3840x2160', 60, 5, 0,  150000);
INSERT INTO MONITOR VALUES (15, 5, 28, '16:9', '3840x2160', 144, 1, 0, 348000);


CREATE TABLE RAKTARINFO(
	monitorId			NUMERIC(4) NOT NULL,
    darabszam           NUMERIC(3),
    rendelheto          BIT,
CONSTRAINT RAKTARINFO_PK PRIMARY KEY (monitorId),
CONSTRAINT RAKTARINFO_FK FOREIGN KEY (monitorId) REFERENCES MONITOR (monitorId)
);

INSERT INTO RAKTARINFO VALUES (1, 5, 0);
INSERT INTO RAKTARINFO VALUES (2, 3, 0);
INSERT INTO RAKTARINFO VALUES (3, 1, 1);
INSERT INTO RAKTARINFO VALUES (13, 0, 1);
INSERT INTO RAKTARINFO VALUES (12, 0, 1);
INSERT INTO RAKTARINFO VALUES (14, 0, 1);
INSERT INTO RAKTARINFO VALUES (10, 0, 1);
INSERT INTO RAKTARINFO VALUES (15, 0, 1);


