﻿// <copyright file="LogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MonitorShop.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using MonitorShop.Data;
    using MonitorShop.Logic;
    using MonitorShop.Repository;

    using Moq;
    using NUnit;
    using NUnit.Framework;

    /// <summary>
    /// The Test class including all the test.
    /// </summary>
    [TestFixture]
    public class LogicTest
    {
        private Mock<IMonitorRepository> monitorMockRepo;
        private Mock<IMonitorbrandRepository> monitorbrandMockRepo;
        private Mock<IRaktarinfoRepository> raktarinfoMockRepo;

        private MonitorLogic monitorLogic;

        /// <summary>
        /// Initializes a mocked database for the tests.
        /// </summary>
        [SetUp]
        public void Init()
        {
            this.monitorbrandMockRepo = new Mock<IMonitorbrandRepository>();
            this.monitorMockRepo = new Mock<IMonitorRepository>();
            this.raktarinfoMockRepo = new Mock<IRaktarinfoRepository>();

            this.monitorbrandMockRepo.Setup(x => x.GetAll()).Returns(new List<MONITORBRAND>()
            {
                new MONITORBRAND() { brandId = 10, brandNev = "ACEL", alapitasEve = 1990, orszag = "HUNGARY" },
                new MONITORBRAND() { brandId = 20, brandNev = "ISUS", alapitasEve = 2005, orszag = "CHINA" },
            });

            /* monitorbrandMockRepo.Setup(x => x.GetById(10)).Returns(new MONITORBRAND() { brandId = 10, brandNev = "ACEL", alapitasEve = 1990, orszag = "HUNGARY" });
            */

            this.monitorMockRepo.Setup(x => x.GetAll()).Returns(new List<MONITOR>()
            {
                new MONITOR() { monitorId = 1, brandId = 10, kepatlo = 24, keparany = "16:9", felbontas = "1920x1080", kepfrissites = 144, valaszido = 1, hajlitott = true, ar = 123000 },
                new MONITOR() { monitorId = 2, brandId = 20, kepatlo = 21, keparany = "16:9", felbontas = "1920x1080", kepfrissites = 60, valaszido = 5, hajlitott = false, ar = 35000 },
            });

            /* monitorMockRepo.Setup(x => x.GetById(1)).Returns(new MONITOR() { monitorId = 1, brandId = 10, kepatlo = 24, keparany = "16:9", felbontas = "1920x1080", kepfrissites = 144, valaszido = 1, hajlitott = true, ar = 123000 });
            */

            this.raktarinfoMockRepo.Setup(x => x.GetAll()).Returns(new List<RAKTARINFO>()
            {
                new RAKTARINFO() { monitorId = 1, darabszam = 5, rendelheto = true },
                new RAKTARINFO() { monitorId = 2, darabszam = 2, rendelheto = false },
            });

            /* raktarinfoMockRepo.Setup(x=>x.GetById(1)).Returns(new RAKTARINFO() { monitorId = 1, darabszam = 5, rendelheto = true });
            */

            this.monitorLogic = new MonitorLogic(this.monitorMockRepo.Object, this.monitorbrandMockRepo.Object, this.raktarinfoMockRepo.Object);
        }

        /// <summary>
        /// Inserts a monitor into a mocked database and verifies that it really happened.
        /// </summary>
        [Test]
        public void AddMonitorTest_Verify()
        {
            MONITOR tmp = new MONITOR() { monitorId = 3, brandId = 10, kepatlo = 17, keparany = "16:9", felbontas = "1920x1080", kepfrissites = 60, valaszido = 3, hajlitott = false, ar = 15000 };
            this.monitorLogic.AddMonitor(3, 10, 17, "16:9", "1920x1080", 60, 3, false, 15000);

            this.monitorMockRepo.Verify(x => x.CreateMonitor(It.IsAny<MONITOR>()), Times.Once);
        }

        /*
        [Test]
        public void GetMonitorByIdTest_Verify()
        {
            var tmp = this.monitorLogic.GetMonitorById(It.IsAny<int>());

            this.monitorMockRepo.Verify(x => x.GetById(It.IsAny<int>()), Times.Once);
        }
        */

        /// <summary>
        /// Updates a monitor in a mocked database and verifies that it really happened.
        /// </summary>
        [Test]
        public void ChangeMonitorPriceTest_Verify()
        {
            this.monitorLogic.ChangeMonitorPrice(1, 50);

            this.monitorMockRepo.Verify(x => x.UpdateAr(1, It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// Deletes a monitorbrand from a mocked database and verifies that it really happened.
        /// </summary>
        [Test]
        public void DeleteMonitorTest_Verify()
        {
            this.monitorLogic.DeleteMonitor(1);
            this.monitorLogic.DeleteMonitor(2);

            this.monitorMockRepo.Verify(x => x.DeleteMonitor(It.IsAny<int>()), Times.Exactly(2));
        }

        /// <summary>
        /// Inserts a monitorbrand into a mocked database and verifies that it really happened.
        /// </summary>
        [Test]
        public void AddMonitorBrandTest_Verify()
        {
            this.monitorLogic.AddMonitorBrand(new MONITORBRAND() { });

            this.monitorbrandMockRepo.Verify(x => x.CreateMonitorBrand(It.IsAny<MONITORBRAND>()), Times.Once);
        }

        /// <summary>
        /// Reads a monitorbrand from a mocked database and verifies that it really happened.
        /// </summary>
        [Test]
        public void GetAllMonitorbrandsTest_Verify()
        {
            this.monitorLogic.GetAllMonitorbrands();

            this.monitorbrandMockRepo.Verify(x => x.GetAll(), Times.Once);
        }

        /// <summary>
        /// Deletes a mocked monitorbrand and verifies that it really happened.
        /// </summary>
        [Test]
        public void DeleteMonitorbrandTest_Verify()
        {
            this.monitorLogic.DeleteMonitorbrand(10);

            this.monitorbrandMockRepo.Verify(x => x.DeleteMonitorBrand(10), Times.Once);
        }

        /// <summary>
        /// Inserts a raktarinfo into a mocked database and verifies that it really happened.
        /// </summary>
        [Test]
        public void AddRaktarInfoTest_Verify()
        {
            this.monitorLogic.AddRaktarInfo(new RAKTARINFO());

            this.raktarinfoMockRepo.Verify(x => x.CreateRaktarinfo(It.IsAny<RAKTARINFO>()), Times.Once);
        }

        /// <summary>
        /// Updates a mocked raktarinfo and verifies that it really happened.
        /// </summary>
        [Test]
        public void ChangeRaktarinfoDarabszamTest_Verify()
        {
            this.monitorLogic.ChangeRaktarinfoDarabszam(10, 15);

            this.raktarinfoMockRepo.Verify(x => x.UpdateDarabszam(10, 15), Times.Once);
        }

        /// <summary>
        /// Updates a mocked raktarinfo and verifies that it really happened.
        /// </summary>
        [Test]
        public void ChangeRaktarinfoRendelheto()
        {
            this.monitorLogic.ChangeRaktarinfoRendelheto(10, false);

            this.raktarinfoMockRepo.Verify(x => x.UpdateRendelheto(10, It.IsAny<bool>()), Times.Once);
        }

        /// <summary>
        /// Deletes a mocked raktarinfo and verifies that it really happened.
        /// </summary>
        [Test]
        public void DeleteRaktarinfo()
        {
            this.monitorLogic.DeleteRaktarinfo(97);

            this.raktarinfoMockRepo.Verify(x => x.DeleteRaktarinfo(97), Times.Once);
        }
    }
}