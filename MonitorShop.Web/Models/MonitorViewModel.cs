﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MonitorShop.Web.Models
{
    public class MonitorViewModel
    {
        public Monitor EditedMonitor { get; set; }
        public List<Monitor> ListOfMonitors { get; set; }
    }
}