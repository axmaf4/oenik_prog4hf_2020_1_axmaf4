﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Security.Policy;
using System.Web;

namespace MonitorShop.Web.Models
{
    // 
    public class Monitor
    {
        [Display(Name = "Monitor ID")]
        [Required]
        public int ID { get; set; }

        [Display(Name = "Brand ID")]
        [Required]
        public int BrandID { get; set; }

        [Display(Name = "Monitor Diagonal")]
        [Required]
        public double Diagonal { get; set; }

        [Display(Name = "Monitor Screen Ratio")]
        [Required]
        public string Ratio { get; set; }

        [Display(Name = "Monitor Resolution")]
        [Required]
        [StringLength(5, MinimumLength = 3)]
        public string Resolution { get; set; }

        [Display(Name = "Monitor Refresh Rate")]
        [Required]
        public int RefreshRate { get; set; }

        [Display(Name = "Monitor Response Time")]
        [Required]
        public int ResponseTime { get; set; }

        [Display(Name = "Monitor Curved")]
        [Required]
        public bool Curved { get; set; }

        [Display(Name = "Monitor Price")]
        [Required]
        public int Price { get; set; }
    }
}