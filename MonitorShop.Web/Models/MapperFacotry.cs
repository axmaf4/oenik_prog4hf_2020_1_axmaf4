﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MonitorShop.Web.Models
{
    public class MapperFacotry
    {
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<MonitorShop.Data.MONITOR, MonitorShop.Web.Models.Monitor>().
                ForMember(dest => dest.ID, map => map.MapFrom(src => src.monitorId)).
                ForMember(dest => dest.BrandID, map => map.MapFrom(src => src.brandId)).
                ForMember(dest => dest.Diagonal, map => map.MapFrom(src => src.kepatlo)).
                ForMember(dest => dest.Ratio, map => map.MapFrom(src => src.keparany)).
                ForMember(dest => dest.Resolution, map => map.MapFrom(src => src.felbontas)).
                ForMember(dest => dest.RefreshRate, map => map.MapFrom(src => src.kepfrissites)).
                ForMember(dest => dest.ResponseTime, map => map.MapFrom(src => src.valaszido)).
                ForMember(dest => dest.Curved, map => map.MapFrom(src => src.hajlitott)).
                ForMember(dest => dest.Price, map => map.MapFrom(src => src.ar));
            });

            return config.CreateMapper();
        }
    }
}