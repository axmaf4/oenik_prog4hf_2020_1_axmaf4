﻿using AutoMapper;
using MonitorShop.Data;
using MonitorShop.Logic;
using MonitorShop.Repository;
using MonitorShop.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MonitorShop.Web.Controllers
{
    public class MonitorsAPIController : ApiController
    {
        public class ApiResult
        {
            public bool OperationResult { get; set; }
        }

        IMonitorLogic logic;
        IMapper mapper;

        public MonitorsAPIController()
        {
            MonitorDatabaseEntities db = new MonitorDatabaseEntities();
            MonitorRepo monitorRepo = new MonitorRepo(db);
            MonitorbrandRepo brandRepo = new MonitorbrandRepo(db);
            RaktarinfoRepo infoRepo = new RaktarinfoRepo(db);

            logic = new MonitorLogic(monitorRepo, brandRepo, infoRepo);
            mapper = MapperFacotry.CreateMapper();
        }


        [ActionName("all")]
        [HttpGet]
        public IEnumerable<Models.Monitor> GetAll()
        {
            var monitors = logic.GetAllMonitorItems();
            return mapper.Map<IEnumerable<Data.MONITOR>, List<Models.Monitor>>(monitors);
        }

        [ActionName("del")]
        [HttpGet]
        public ApiResult DelOneMonitor(int id)
        {
            try
            {
                this.logic.DeleteMonitor(id);
            }
            catch (Exception)
            {
                return new ApiResult() { OperationResult = false };
            }
            return new ApiResult() { OperationResult = true };
        }

        [ActionName("add")]
        [HttpPost]
        public ApiResult AddOneMonitor(Monitor monitor)
        {
            try
            {
                this.logic.AddMonitor(monitor.ID, monitor.BrandID, monitor.Diagonal, monitor.Ratio,
                        monitor.Resolution, monitor.RefreshRate, monitor.ResponseTime,monitor.Curved, monitor.Price);
            }
            catch (Exception)
            {
                return new ApiResult() { OperationResult = false };
            }
            return new ApiResult() { OperationResult = true };
        }

        [ActionName("mod")]
        [HttpPost]
        public ApiResult ModOneCar(Monitor monitor)
        {
            try
            {
                this.logic.ChangeMonitor(monitor.ID, monitor.Diagonal, monitor.Ratio,
                            monitor.Resolution, monitor.RefreshRate, monitor.ResponseTime, monitor.Curved, monitor.Price);
            }
            catch (Exception)
            {
                return new ApiResult() { OperationResult = false };
            }
            return new ApiResult() { OperationResult = true };
        }
    }
}
