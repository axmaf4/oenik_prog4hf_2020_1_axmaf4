﻿using AutoMapper;
using MonitorShop.Logic;
using MonitorShop.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using MonitorShop.Data;
using MonitorShop.Repository;
using System.ComponentModel.DataAnnotations;

namespace MonitorShop.Web.Controllers
{
    public class MonitorsController : Controller
    {
        IMonitorLogic logic;
        IMapper mapper;
        MonitorViewModel vm;
        
        public MonitorsController()
        {
            MonitorDatabaseEntities db = new MonitorDatabaseEntities();
            MonitorRepo monitorRepo = new MonitorRepo(db);
            MonitorbrandRepo brandRepo = new MonitorbrandRepo(db);
            RaktarinfoRepo infoRepo = new RaktarinfoRepo(db);

            logic = new MonitorLogic(monitorRepo, brandRepo, infoRepo);
            mapper = MapperFacotry.CreateMapper();
            vm = new MonitorViewModel();

            vm.EditedMonitor = new Monitor();
            var monitors = logic.GetAllMonitorItems();
            vm.ListOfMonitors = mapper.Map<IEnumerable<Data.MONITOR>, List<Models.Monitor>>(monitors);
        }

        private Monitor GetMonitorModel(int id)
        {
            MONITOR monitor = logic.GetMonitorItemById(id);
            return mapper.Map<Data.MONITOR, Models.Monitor>(monitor);
        }

        // GET: Monitors
        public ActionResult Index()
        {
            ViewData["editAction"] = "AddNew";
            return View("MonitorIndex", vm);
        }

        // GET: Monitors/Details/5
        public ActionResult Details(int id)
        {
            return View("MonitorDetails", GetMonitorModel(id));
        }

        public ActionResult Remove(int id)
        {
            TempData["editResult"] = "Delete OK";
            try
            {
                logic.DeleteMonitor(id);
            }
            catch (Exception)
            {
                TempData["editResult"] = "Delete FAIL";
            }
            return RedirectToAction(nameof(Index));
        }

        public ActionResult Edit(int id)
        {
            ViewData["editAction"] = "Edit";
            vm.EditedMonitor = GetMonitorModel(id);
            return View("MonitorIndex", vm);
        }

        [HttpPost]
        public ActionResult Edit(Monitor monitor, string editAction)
        {
            if (ModelState.IsValid && monitor != null)
            {
                TempData["editResult"] = "Edit OK";

                if (editAction == "AddNew")
                {
                    try
                    {
                        this.logic.AddMonitor(monitor.ID, monitor.BrandID, monitor.Diagonal, monitor.Ratio,
                        monitor.Resolution, monitor.RefreshRate, monitor.ResponseTime, monitor.Curved, monitor.Price);
                    }
                        catch (Exception)
                    {
                        TempData["editResult"] = "Edit FAIL";
                    }
            }
                else
                {
                    try
                    {
                        this.logic.ChangeMonitor(monitor.ID, monitor.Diagonal, monitor.Ratio,
                            monitor.Resolution, monitor.RefreshRate, monitor.ResponseTime, monitor.Curved, monitor.Price);
                    }
                    catch (Exception)
                    {
                        TempData["editResult"] = "Edit FAIL";
                    }
                }
                return RedirectToAction(nameof(Index));

            }
            else
            {
                ViewData["editAction"] = "Edit";
                vm.EditedMonitor = monitor;
                return View("MonitorIndex", vm);
            }
        }

    }
}
